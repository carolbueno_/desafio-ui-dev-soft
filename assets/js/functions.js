let dropArea = document.getElementById('drop-area');
let modalContainer = document.getElementById('modal-upload-files');
let uploadedWrapper = document.getElementById('uploaded-wrapper');
let uploadedArea = document.getElementById('uploaded-files');

function dropHandler(ev) {

    ev.preventDefault();

    if(ev.dataTransfer.items.length > 0) {
        $(dropArea).hide();
        $(uploadedWrapper).show();
    }

    for (let i = 0; i < ev.dataTransfer.items.length; i++) {

        let file = ev.dataTransfer.items[i].getAsFile();
        let txt_filename = document.createTextNode(file.name);

        $(uploadedArea).append(
            '<div class="file-wrapper">' +
                '<p>' + txt_filename.data + '</p>' +
                '<i class="material-icons remove-file">delete</i>'+
                '<div class="clear"></div>' +
                '<div class="progress-container" data-width="100%">' +
                    '<div class="progress-bar" data-name="CSS"></div>' +
                '</div>' +
            '</div>'
        );

        $('.progress-container').each(function(){
            loadProgressBar($(this).find("div"), $(this).data("width"))
        });

    }

}
function uploadBtn(ev){

    if (ev.target.files.length > 0) {
        $(dropArea).hide();
        $(uploadedWrapper).show();
    }

    for (let i = 0; i < ev.target.files.length; i++ ){

        let file = ev.target.files[i];
        let txt_filename = document.createTextNode(file.name);

        $(uploadedArea).append(
            '<div class="file-wrapper">' +
                '<p>' + txt_filename.data + '</p>' +
                '<i class="material-icons remove-file">delete</i>'+
                '<div class="clear"></div>' +
                '<div class="progress-container" data-width="100%">' +
                    '<div class="progress-bar" data-name="CSS"></div>' +
                '</div>' +
            '</div>'
        );

        $('.progress-container').each(function(){
            loadProgressBar($(this).find("div"), $(this).data("width"));
        });
    }
}

;['dragenter', 'dragover'].forEach(eventName => {
    dropArea.addEventListener(eventName, activeDrag, false);
})

;['dragleave', 'drop'].forEach(eventName => {
    dropArea.addEventListener(eventName, inactiveDrag, false);
})

;['drop', 'dragover'].forEach(eventName => {
    modalContainer.addEventListener(eventName, dragOverHandler, false);
})

function activeDrag(ev) {
    dropArea.classList.add('active-drag');
}
function inactiveDrag(ev) {
    dropArea.classList.remove('active-drag');
}

function dragOverHandler(ev) {
    ev.preventDefault();
}

// funcao para gerar progress bar
function loadProgressBar(el, width){
    el.animate(
        {width: width},
        {
            duration: 2000,
            step: function(now, fx) {}
        }
    );
}

$('body').on('click', '#drop-area #upload_link', function(ev){
    ev.preventDefault();
    $("#drop-area #upload-btn:hidden").trigger('click');
});

$('body').on('click', '.btn-wrapper #upload_link', function(ev){
    ev.preventDefault();
    $(".btn-wrapper #continue-upload-btn:hidden").trigger('click');
});

$('body').on('click', '.remove-file', function () {
    $(this).parent().remove();

    if ($(uploadedArea).is(':empty')){
        $(uploadedWrapper).hide();
        $(dropArea).show();
    }
});

$('.icons-container img, .icons-container, .icons-container .wrapper, #upload_link, #drop-area').on('dragstart', function(ev) {
    ev.stopImmediatePropagation();
    ev.preventDefault();
});
